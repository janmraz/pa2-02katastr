#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;
#endif /* __PROGTEST__ */

class Owner;
class City;
class Region;

class Record {
public:
    string city_name;
    string region_name;
    string address_name;
    unsigned record_id;
    Owner * owner;
    Region * region;
    City * city;
    Record(const string & city,const string & region,const string & addr, unsigned id, Owner * def_owner):
        city_name(city), region_name(region), address_name(addr), record_id(id), owner(def_owner), region(NULL), city(NULL) { }
    void changeOwner(Owner * new_owner){
        owner = new_owner;
    }
    void changeCity(City * new_city){
        city = new_city;
    }
    void changeRegion(Region * new_region){
        region = new_region;
    }
};
class City {
public:
    string city_name;
    list<Record*> records;
    City(const string & city): city_name(city) { }
    void addRecord(Record * record) {
        records.push_back(record);
    }
    bool del(const Record & record){
        list<Record*>::iterator record_iter;
        for ( record_iter=records.begin(); record_iter!=records.end(); ++record_iter){
            if ((*record_iter)->address_name == record.address_name
                && (*record_iter)->city_name == record.city_name) {
                records.erase(record_iter++);
                return true;
            }
        }
        return false;
    }
};
class Region {
public:
    string region_name;
    list<Record*> records;
    Region(const string & name): region_name(name) { }
    void addRecord(Record * record){
        records.push_back(record);
    }
    bool del(const Record & record){
        list<Record*>::iterator record_iter;
        for ( record_iter=records.begin(); record_iter!=records.end(); ++record_iter){
            if ((*record_iter)->region_name == record.region_name
                && (*record_iter)->record_id == record.record_id) {
                records.erase(record_iter++);
                return true;
            }
        }
        return false;
    }
};
class Owner {
public:
    string owner_name;
    list<Record*> records;
    Owner(const string & name): owner_name(name) { }
    void addRecord(Record *record){
        records.push_back(record);
    }
    bool del(const Record & record){
        list<Record*>::iterator record_iter;
        for ( record_iter=records.begin(); record_iter!=records.end(); ++record_iter){
            if ((*record_iter)->address_name == record.address_name
                && (*record_iter)->city_name == record.city_name) {
                records.erase(record_iter++);
                return true;
            }
        }
        return false;
    }
    bool static compareNames(const string& name1, const string& name2) {
        return((name1.size() == name2.size()) && equal(name1.begin(), name1.end(), name2.begin(), [](char a, char b){
            return(toupper(a) == toupper(b));
        }));
    }
};

class CIterator {
public:
    bool AtEnd(void) const;

    void Next(void);

    string City(void) const;

    string Addr(void) const;

    string Region(void) const;

    unsigned ID(void) const;

    string Owner(void) const;

    CIterator(const vector<Record*> & records);

private:
    int index;
    const vector<Record*> m_records;
};

string CIterator::City(void) const {
    return m_records[index]->city_name;
}

string CIterator::Region(void) const {
    return m_records[index]->region_name;
}

string CIterator::Addr(void) const {
    return m_records[index]->address_name;
}

unsigned CIterator::ID(void) const {
    return m_records[index]->record_id;
}

string CIterator::Owner(void) const {
    return m_records[index]->owner == NULL ? "" : m_records[index]->owner->owner_name;
}

CIterator::CIterator(const vector<Record*> & records): m_records(records) {
    index = 0;
}

void CIterator::Next(void) {
    index++;
}

bool CIterator::AtEnd(void) const {
    return index == (int) m_records.size();
}

class CLandRegister {
public:
    bool Add(const string &city,
             const string &addr,
             const string &region,
             unsigned int id);

    bool Del(const string &city,
             const string &addr);

    bool Del(const string &region,
             unsigned int id);

    bool GetOwner(const string &city,
                  const string &addr,
                  string &owner) const;

    bool GetOwner(const string &region,
                  unsigned int id,
                  string &owner) const;

    bool NewOwner(const string &city,
                  const string &addr,
                  const string &owner);

    bool NewOwner(const string &region,
                  unsigned int id,
                  const string &owner);

    unsigned Count(const string &owner) const;

    CIterator ListByAddr(void) const;

    CIterator ListByOwner(const string &owner) const;
    CLandRegister();
private:

    bool addCity(const Record & record);
    Region * fetchOrCreateRegion(const string &region);
    Owner * fetchOrCreateOwner(const string &owner);

    bool checkExistence(const Region &region, unsigned int id);

    list<Region> m_regions;
    list<City> m_cities;
    list<Record> m_records;
    list<Owner> m_owners;
    Owner * no_owner;
};

CLandRegister::CLandRegister(){
    Owner owner("");
    m_owners.push_back(owner);
    no_owner = &m_owners.back();
}

bool CLandRegister::Add(const string &city, const string &addr, const string &region, unsigned int id) {
    Record record(city,region, addr, id, no_owner);
    return addCity(record);
}

bool CLandRegister::addCity(const Record & record) {
    bool newCity = m_cities.empty();
    City * city_ptr = nullptr;

    list<City>::iterator city_iter;
    for (city_iter=m_cities.begin(); city_iter!=m_cities.end(); ++city_iter){
        if (city_iter->city_name == record.city_name){
            city_ptr = &(*city_iter);
            break;
        }else if(city_iter->city_name > record.city_name){
            newCity = true;
            break;
        }
    }
    // found or create region
    Region * region_ptr = fetchOrCreateRegion(record.region_name);
    if(checkExistence(*region_ptr,record.record_id))
        return false;

    if(newCity){
        City new_city(record.city_name);
        m_cities.insert(city_iter,new_city);
        city_ptr = m_cities.empty() ? &( m_cities.back()) : &(*(--city_iter));
    }

    m_records.push_back(record);

    region_ptr->addRecord(&m_records.back());
    no_owner->addRecord(&m_records.back());

    if(city_ptr == nullptr){
        City new_city(record.city_name);
        m_cities.insert(city_iter,new_city);
        city_ptr = m_cities.empty() ? &( m_cities.back()) : &(*(--city_iter));
    }

    if(city_ptr->records.empty()){
        city_ptr->addRecord(&m_records.back());
    } else {
        list<Record*>::iterator record_iter;
        bool isLast = true;
        for (record_iter = city_ptr->records.begin(); record_iter != city_ptr->records.end(); ++record_iter){
            if((*record_iter)->address_name == record.address_name){
                return false;
            }else if((*record_iter)->address_name > record.address_name){ // TODO check
                city_ptr->records.insert(record_iter,&m_records.back());
                isLast = false;
                break;
            }
        }
        if(isLast)
            city_ptr->records.insert(record_iter,&m_records.back());
    }

    m_records.back().changeCity(city_ptr);
    m_records.back().changeRegion(region_ptr);

    return true;
}

Region * CLandRegister::fetchOrCreateRegion(const string &region) {
    for (auto &m_region : m_regions)
        if (m_region.region_name == region)
            return &m_region;
    Region new_region(region);
    m_regions.push_back(new_region);
    Region * region_ptr = &m_regions.back();
    return region_ptr;
}
Owner * CLandRegister::fetchOrCreateOwner(const string &owner) {
    for (auto &m_owner : m_owners) {
        if (m_owner.owner_name == owner)
            return &m_owner;
    }
    Owner new_owner(owner);
    m_owners.push_back(new_owner);
    return &m_owners.back();
}

unsigned CLandRegister::Count(const string &owner) const {
    unsigned int result = 0;
    for (const auto &m_owner : m_owners)
        if (Owner::compareNames(m_owner.owner_name, owner))
            result += m_owner.records.size();
    return result;
}

CIterator CLandRegister::ListByAddr(void) const {
    vector<Record*> recs;
    for(const City &city: m_cities)
        for(Record * record: city.records)
            recs.push_back(record);
    return CIterator(recs);
}

bool CLandRegister::Del(const string &city, const string &addr) {
    bool deleted = false;
    const Record * recordPtr;
    for (auto &m_city : m_cities)
        if (m_city.city_name == city) {
            list<Record *>::iterator record_iter;
            for (record_iter = m_city.records.begin(); record_iter != m_city.records.end(); ++record_iter) {
                if ((*record_iter)->address_name == addr) {
                    recordPtr = (*record_iter);

                    (*record_iter)->region->del(*recordPtr);
                    (*record_iter)->owner->del(*recordPtr);
                    m_city.records.erase(record_iter++);

                    deleted = true;
                }

            }
        }

    return deleted;
}

bool CLandRegister::Del(const string &region, unsigned int id) {
    bool deleted = false;
    Record *recordPtr;
    for (auto &m_region : m_regions)
        if (m_region.region_name == region) {
            list<Record *>::iterator record_iter;
            for (record_iter = m_region.records.begin(); record_iter != m_region.records.end(); ++record_iter) {
                if ((*record_iter)->record_id == id) {
                    recordPtr = (*record_iter);

                    (*record_iter)->city->del(*recordPtr);
                    (*record_iter)->owner->del(*recordPtr);
                    m_region.records.erase(record_iter++);
                    deleted = true;
                    break;
                }
            }

        }
    return deleted;
}

bool CLandRegister::NewOwner(const string &city, const string &addr, const string &owner) {
    bool found = false;
    Owner * owner1 = fetchOrCreateOwner(owner);

    for (auto &m_city : m_cities)
        if (m_city.city_name == city){
            list<Record *>::iterator record_iter;
            for (record_iter = m_city.records.begin(); record_iter != m_city.records.end(); ++record_iter)
                if((*record_iter)->address_name == addr){
                    if((*record_iter)->owner->owner_name == owner)
                        return false;

                    (*record_iter)->owner->del(*(*record_iter));
                    owner1->addRecord((*record_iter));
                    (*record_iter)->changeOwner(owner1);

                    return true;
                }
        }

    return found;
}

bool CLandRegister::NewOwner(const string &region, unsigned int id, const string &owner) {
    bool found = false;
    Owner * owner1 = fetchOrCreateOwner(owner);

    for (auto &m_region : m_regions)
        if (m_region.region_name == region) {
            list<Record *>::iterator record_iter;
            for (record_iter = m_region.records.begin(); record_iter != m_region.records.end(); ++record_iter)
                if ((*record_iter)->record_id == id) {
                    if ((*record_iter)->owner->owner_name == owner)
                        return false;

                    (*record_iter)->owner->del(*(*record_iter));
                    owner1->addRecord((*record_iter));
                    (*record_iter)->changeOwner(owner1);

                    return true;
                }

        }
    return found;
}

CIterator CLandRegister::ListByOwner(const string &owner) const {
    vector<Record*> recs;
    for (const auto &m_owner : m_owners)
        if(Owner::compareNames(m_owner.owner_name, owner))
            for(Record * record: m_owner.records)
                recs.push_back(record);
    return CIterator(recs);
}

bool CLandRegister::GetOwner(const string &city,
              const string &addr,
              string &owner) const{
    bool found = false;
    for (const auto &m_citie : m_cities)
        if (m_citie.city_name == city)
            for (const auto &record : m_citie.records)
                if(record->address_name == addr){
                    found = true;
                    owner = record->owner->owner_name;
                }
    return found;
}

bool CLandRegister::GetOwner(const string &region,
              unsigned int id,
              string &owner) const{
    bool found = false;
    for (const auto &m_region : m_regions)
        if (m_region.region_name == region)
            for (const auto &record : m_region.records)
                if(record->record_id == id){
                    found = true;
                    owner = record->owner->owner_name;
                }
    return found;
}

bool CLandRegister::checkExistence(const Region & region, unsigned int id) {
    for (auto record : region.records)
        if(record->record_id == id)
            return true;
    return false;
}

#ifndef __PROGTEST__

static void test0(void) {
    CLandRegister x;
    string owner;

    assert (x.Add("Prague", "Thakurova", "Dejvice", 12345));
    assert (x.Add("Prague", "Evropska", "Vokovice", 12345));
    assert (x.Add("Prague", "Technicka", "Dejvice", 9873));
    assert (x.Add("Prague", "ZBorislavka", "Dejvice", 4567));
    assert (x.Del("Prague", "ZBorislavka"));
    assert (x.Add("Plzen", "Evropska", "Plzen mesto", 78901));
    assert (x.Add("Liberec", "Evropska", "Librec", 4552));
    assert (x.Add("Zlin", "Evropska", "Zlin", 1234));
    assert (x.Del("Zlin", "Evropska"));
    assert (!x.NewOwner("Dejvice", 1322, "CVUT"));
    CIterator i0 = x.ListByAddr();
    assert (!i0.AtEnd()
            && i0.City() == "Liberec"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Librec"
            && i0.ID() == 4552
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Plzen"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Plzen mesto"
            && i0.ID() == 78901
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Vokovice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Technicka"
            && i0.Region() == "Dejvice"
            && i0.ID() == 9873
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Thakurova"
            && i0.Region() == "Dejvice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (i0.AtEnd());

    assert (x.Count("") == 5);
    CIterator i1 = x.ListByOwner("");
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Thakurova"
            && i1.Region() == "Dejvice"
            && i1.ID() == 12345
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Evropska"
            && i1.Region() == "Vokovice"
            && i1.ID() == 12345
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Technicka"
            && i1.Region() == "Dejvice"
            && i1.ID() == 9873
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Plzen"
            && i1.Addr() == "Evropska"
            && i1.Region() == "Plzen mesto"
            && i1.ID() == 78901
            && i1.Owner() == "");
    i1.Next();
    assert (!i1.AtEnd()
            && i1.City() == "Liberec"
            && i1.Addr() == "Evropska"
            && i1.Region() == "Librec"
            && i1.ID() == 4552
            && i1.Owner() == "");
    i1.Next();
    assert (i1.AtEnd());

    assert (x.Count("CVUT") == 0);
    CIterator i2 = x.ListByOwner("CVUT");
    assert (i2.AtEnd());

    assert (x.NewOwner("Prague", "Thakurova", "CVUT"));
    assert (x.NewOwner("Dejvice", 9873, "CVUT"));
    assert (x.NewOwner("Plzen", "Evropska", "Anton Hrabis"));
    assert (x.NewOwner("Librec", 4552, "Cvut"));
    assert (x.GetOwner("Prague", "Thakurova", owner) && owner == "CVUT");
    assert (x.GetOwner("Dejvice", 12345, owner) && owner == "CVUT");
    assert (x.GetOwner("Prague", "Evropska", owner) && owner == "");
    assert (x.GetOwner("Vokovice", 12345, owner) && owner == "");
    assert (x.GetOwner("Prague", "Technicka", owner) && owner == "CVUT");
    assert (x.GetOwner("Dejvice", 9873, owner) && owner == "CVUT");
    assert (x.GetOwner("Plzen", "Evropska", owner) && owner == "Anton Hrabis");
    assert (x.GetOwner("Plzen mesto", 78901, owner) && owner == "Anton Hrabis");
    assert (x.GetOwner("Liberec", "Evropska", owner) && owner == "Cvut");
    assert (x.GetOwner("Librec", 4552, owner) && owner == "Cvut");
    CIterator i3 = x.ListByAddr();
    assert (!i3.AtEnd()
            && i3.City() == "Liberec"
            && i3.Addr() == "Evropska"
            && i3.Region() == "Librec"
            && i3.ID() == 4552
            && i3.Owner() == "Cvut");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Plzen"
            && i3.Addr() == "Evropska"
            && i3.Region() == "Plzen mesto"
            && i3.ID() == 78901
            && i3.Owner() == "Anton Hrabis");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Prague"
            && i3.Addr() == "Evropska"
            && i3.Region() == "Vokovice"
            && i3.ID() == 12345
            && i3.Owner() == "");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Prague"
            && i3.Addr() == "Technicka"
            && i3.Region() == "Dejvice"
            && i3.ID() == 9873
            && i3.Owner() == "CVUT");
    i3.Next();
    assert (!i3.AtEnd()
            && i3.City() == "Prague"
            && i3.Addr() == "Thakurova"
            && i3.Region() == "Dejvice"
            && i3.ID() == 12345
            && i3.Owner() == "CVUT");
    i3.Next();
    assert (i3.AtEnd());

    assert (x.Count("cvut") == 3);
    CIterator i4 = x.ListByOwner("cVuT");
    assert (!i4.AtEnd()
            && i4.City() == "Prague"
            && i4.Addr() == "Thakurova"
            && i4.Region() == "Dejvice"
            && i4.ID() == 12345
            && i4.Owner() == "CVUT");
    i4.Next();
    assert (!i4.AtEnd()
            && i4.City() == "Prague"
            && i4.Addr() == "Technicka"
            && i4.Region() == "Dejvice"
            && i4.ID() == 9873
            && i4.Owner() == "CVUT");
    i4.Next();
    assert (!i4.AtEnd()
            && i4.City() == "Liberec"
            && i4.Addr() == "Evropska"
            && i4.Region() == "Librec"
            && i4.ID() == 4552
            && i4.Owner() == "Cvut");
    i4.Next();
    assert (i4.AtEnd());

    assert (x.NewOwner("Plzen mesto", 78901, "CVut"));
    assert (x.Count("CVUT") == 4);
    CIterator i5 = x.ListByOwner("CVUT");
    assert (!i5.AtEnd()
            && i5.City() == "Prague"
            && i5.Addr() == "Thakurova"
            && i5.Region() == "Dejvice"
            && i5.ID() == 12345
            && i5.Owner() == "CVUT");
    i5.Next();
    assert (!i5.AtEnd()
            && i5.City() == "Prague"
            && i5.Addr() == "Technicka"
            && i5.Region() == "Dejvice"
            && i5.ID() == 9873
            && i5.Owner() == "CVUT");
    i5.Next();
    assert (!i5.AtEnd()
            && i5.City() == "Liberec"
            && i5.Addr() == "Evropska"
            && i5.Region() == "Librec"
            && i5.ID() == 4552
            && i5.Owner() == "Cvut");
    i5.Next();
    assert (!i5.AtEnd()
            && i5.City() == "Plzen"
            && i5.Addr() == "Evropska"
            && i5.Region() == "Plzen mesto"
            && i5.ID() == 78901
            && i5.Owner() == "CVut");
    i5.Next();
    assert (i5.AtEnd());

    assert (x.Del("Liberec", "Evropska"));
    assert (x.Del("Plzen mesto", 78901));
    assert (x.Count("cvut") == 2);
    CIterator i6 = x.ListByOwner("cVuT");
    assert (!i6.AtEnd()
            && i6.City() == "Prague"
            && i6.Addr() == "Thakurova"
            && i6.Region() == "Dejvice"
            && i6.ID() == 12345
            && i6.Owner() == "CVUT");
    i6.Next();
    assert (!i6.AtEnd()
            && i6.City() == "Prague"
            && i6.Addr() == "Technicka"
            && i6.Region() == "Dejvice"
            && i6.ID() == 9873
            && i6.Owner() == "CVUT");
    i6.Next();
    assert (i6.AtEnd());

    assert (x.Add("Liberec", "Evropska", "Librec", 4552));
}

static void test1(void) {
    CLandRegister x;
    string owner;

    assert (x.Add("Prague", "Thakurova", "Dejvice", 12345));
    assert (x.Add("Prague", "Evropska", "Vokovice", 12345));
    assert (x.Add("Prague", "Technicka", "Dejvice", 9873));
    assert (!x.Add("Prague", "Technicka", "Hradcany", 7344));
    assert (!x.Add("Brno", "Bozetechova", "Dejvice", 9873));
    assert (!x.GetOwner("Prague", "THAKUROVA", owner));
    assert (!x.GetOwner("Hradcany", 7343, owner));
    CIterator i0 = x.ListByAddr();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Evropska"
            && i0.Region() == "Vokovice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Technicka"
            && i0.Region() == "Dejvice"
            && i0.ID() == 9873
            && i0.Owner() == "");
    i0.Next();
    assert (!i0.AtEnd()
            && i0.City() == "Prague"
            && i0.Addr() == "Thakurova"
            && i0.Region() == "Dejvice"
            && i0.ID() == 12345
            && i0.Owner() == "");
    i0.Next();
    assert (i0.AtEnd());

    assert (x.NewOwner("Prague", "Thakurova", "CVUT"));
    assert (!x.NewOwner("Prague", "technicka", "CVUT"));
    assert (!x.NewOwner("prague", "Technicka", "CVUT"));
    assert (!x.NewOwner("dejvice", 9873, "CVUT"));
    assert (!x.NewOwner("Dejvice", 9973, "CVUT"));
    assert (!x.NewOwner("Dejvice", 12345, "CVUT"));
    assert (x.Count("CVUT") == 1);
    CIterator i1 = x.ListByOwner("CVUT");
    assert (!i1.AtEnd()
            && i1.City() == "Prague"
            && i1.Addr() == "Thakurova"
            && i1.Region() == "Dejvice"
            && i1.ID() == 12345
            && i1.Owner() == "CVUT");
    i1.Next();
    assert (i1.AtEnd());

    assert (!x.Del("Brno", "Technicka"));
    assert (!x.Del("Karlin", 9873));
    assert (x.Del("Prague", "Technicka"));
    assert (!x.Del("Prague", "Technicka"));
    assert (!x.Del("Dejvice", 9873));
}

int main(void) {
    test0();
    test1();
    return 0;
}

#endif /* __PROGTEST__ */
